package com.company;

public class Main
{
    final int N=4;
    boolean issafe(int maze[][],int row,int col)
    {
        return (row<N && row >= 0 && col < N && col >=0 && maze[row][col] == 1);
    }
    void printSolution(int sol[][])
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
                System.out.print(" " + sol[i][j] +
                        " ");
            System.out.println();
        }
    }
    boolean solvemaze(int maze[][])
    {
        int sol[][]={ {0,0,0,0},
                {0,0,0,0},
                {0,0,0,0},
                {0,0,0,0}
        };
        if(mazeutil(maze,0,0,sol) == false)
        {
            System.out.println("the path is not found");
            return false;
        }
        printSolution(sol);
        return true;
    }
    boolean mazeutil(int maze[][],int row,int col,int sol[][])
    {
        if (row == N-1 && col ==N-1)
        {
            sol[row][col]=1;
            return true;
        }
        if(issafe(maze,row,col)==true)
        {
            sol[row][col]=1;
            if(mazeutil(maze,row + 1,col,sol) == true)
            {
                return true;
            }
            if(mazeutil(maze,row,col+1,sol) == true)
            {
                return true;
            }
            sol[row][col]=0;
            return false;
        }
        return false;
    }
    public static void main(String[] args)
    {
        Main s1=new Main();
        int maze[][]  =  { {1, 0, 0, 0},
        {1, 1, 1, 1},
        {0, 1, 0, 0},
        {1, 1, 1, 1}
    };
        s1.solvemaze(maze);
    }
}
