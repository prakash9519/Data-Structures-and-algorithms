package com.company;


public class LinkedList
{
    Node head;
    static class Node
    {
        int data;
        Node next;

        public Node(int d)
        {
            data = d;
            next=null;
        }
    }
    public static LinkedList insertAtlast(LinkedList list, int data)
    {
        Node new_node=new Node(data);
        new_node.next=null;

        if(list.head==null)
        {
            list.head=new_node;
        }
        else
        {
            Node last=list.head;
            while (last.next != null)
            {
                last=last.next;
            }
            last.next=new_node;
        }
        return list;
    }
    public static LinkedList insertAtFirst(LinkedList list,int data)
    {
        Node new_node=new Node(data);
        new_node.next=list.head;

        list.head=new_node;
        return list;
    }
    public static LinkedList insertAtmiddle(LinkedList list,Node prev_node,int data)
    {
        if(prev_node == null)
        {
            System.out.println("The given previous node cannot be null");
        }
        Node new_node=new Node(data);
        new_node.next=prev_node.next;
        prev_node.next=new_node;
        return list;
    }
    public static void printlist(LinkedList list)
    {
        Node cur_node=list.head;
        while (cur_node != null)
        {
            System.out.println(" " + cur_node.data + " ");
            cur_node=cur_node.next;
        }
    }
    public static void main(String[] args)
    {
        LinkedList list=new LinkedList();
        list=insertAtlast(list,1);
        list=insertAtlast(list,2);
        list=insertAtFirst(list,3);
        list=insertAtmiddle(list,list.head.next,5);
        printlist(list);
    }
}
