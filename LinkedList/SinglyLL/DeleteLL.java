package com.company;

public class Main
{
    Node head;
    static class Node
    {
        int data;
        Node next;

        public Node(int data)
        {
            this.data = data;
            this.next=null;
        }
    }
    public void insertAtLast(int data)
    {
        Node new_node=new Node(data);
        new_node.next=null;
        if (head==null)
        {
            head=new Node(data);
            return;
        }
        Node temp=head;
        while (temp.next != null)
        {
            temp=temp.next;
        }
        temp.next=new_node;
        return;
    }
    public void deleteAtLast()
    {
        Node prev=null;
        Node temp=head;
        while (temp.next != null)
        {
            prev=temp;
            temp=temp.next;
        }
        prev.next=null;
    }
    public void deleteAtmiddle(int key)
    {
        Node prev=null;
        Node temp=head;
        while(temp != null && temp.data != key)
        {
            prev=temp;
            temp=temp.next;
        }
        prev.next=temp.next;
    }
    public void deleteAtFirst()
    {
        Node temp=head;
        head=head.next;
        temp=null;
    }
    public void print()
    {
        Node temp=head;
        while(temp != null)
        {
            System.out.println(temp.data);
            temp=temp.next;
        }
    }
    public static void main(String[] args)
    {
        Main list=new Main();
        list.insertAtLast(1);
        list.insertAtLast(2);
        list.insertAtLast(3);
        list.insertAtLast(4);
        list.insertAtLast(5);
        list.insertAtLast(6);
        list.insertAtLast(7);
        System.out.println("Linked List :");
        list.print();
        System.out.println("Linked List DeleteAtLast :");
        list.deleteAtLast();
        list.print();
        System.out.println("Linked List DeleteAtFirst :");
        list.deleteAtFirst();
        list.print();
        System.out.println("Linked List DeleteAtMiddle :");
        list.deleteAtmiddle(3);
        list.print();
    }
}
